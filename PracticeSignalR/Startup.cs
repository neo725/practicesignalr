﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(PracticeSignalR.Startup))]
namespace PracticeSignalR
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}