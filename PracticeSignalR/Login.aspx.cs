﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PracticeSignalR
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var isLogin = HttpContext.Current.User.Identity.IsAuthenticated;

                pnlLogined.Visible = isLogin;
                pnlNotLogin.Visible = !isLogin;

                if (isLogin)
                {
                    lblAccount.Text = HttpContext.Current.User.Identity.Name;
                }
                else
                {
                    txtAccountId.Text = "";
                }
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SetAuthCookie(txtAccountId.Text.Trim(), true);
            Response.Redirect("Login.aspx");
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("Login.aspx");
        }
    }
}