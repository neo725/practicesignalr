﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignalR1.aspx.cs" Inherits="PracticeSignalR.SignalR1" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <input type="text" id="message"/>
        <input type="button" id="sendMessage" value="send"/>
    </div>
    <div id="messagePanel">
    
    </div>
    </form>
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/jquery.signalR-2.1.1.min.js"></script>
    <%: Scripts.Render("~/signalr/hubs") %>
    <script type="text/javascript">
        $(function () {
            var chatHub = $.connection.ChatHub;

            chatHub.client.broadcast = function (message) {
                showMessage(message);
            };

            $('#sendMessage').click(function () {
                var message = $('#message').val();

                chatHub.server.sendMessage(message);

                $('#message').val('');
            });

            var showMessage = function (message) {
                $('#messagePanel').html($('#messagePanel').html() + message + '<br />');
            };

            $.connection.hub.start();
        });
    </script>
</body>
</html>
