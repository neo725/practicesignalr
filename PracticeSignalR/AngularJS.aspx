﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AngularJS.aspx.cs" Inherits="PracticeSignalR.AngularJS" %>

<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" ng-app="Demo">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div ng-controller="DemoCtrl">
            <input type="text" id="message" ng-model="message" />
            <input type="button" id="sendMessage" value="send" ng-click="send()" />
        </div>
        <div ng-controller="Demo2Ctrl">
            <input type="text" id="message" ng-model="message" />
            <input type="button" id="sendMessage" value="send" ng-click="send()" />
        </div>
        <div ng-controller="Demo3Ctrl">
            <input type="text" id="message" ng-model="message" />
            <input type="button" id="sendMessage" value="send" ng-click="send()" />
        </div>
    </form>
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/jquery.signalR-2.1.1.min.js"></script>
    <script src="Scripts/angular.min.js"></script>
    <script src="Scripts/angular-signalr-hub.js"></script>
    <%: Scripts.Render("~/signalr/hubs") %>
    <script type="text/javascript">
        // use SignalR version
        //$(function () {
        //    var angularHub = $.connection.AngularHub;

        //    angularHub.client.broadcast = function (message) {
        //        console.log(message);
        //    };

        //    $('#sendMessage').click(function () {
        //        var message = $('#message').val();

        //        angularHub.server.sendMessage(message);

        //        $('#message').val('');
        //    });

        //    $.connection.hub.start();
        //});
        // use AngularJS version
        var ngapp = angular.module('Demo', ['SignalR']);
        ngapp.factory('AngularHub', [
            'Hub', function (Hub) {
                return function (sharedConnection) {
                    //declaring the hub connection
                    var hub = new Hub('AngularHub', {

                        ////client side methods
                        //listeners: {
                        //    'broadcast': function (message) {
                        //        console.log(message);
                        //    }
                        //},

                        //server side methods
                        methods: ['sendMessage'],

                        ////query params sent on initial connection
                        //queryParams: {
                        //    'token': 'exampletoken'
                        //},

                        useSharedConnection: sharedConnection === undefined ? false : sharedConnection,

                        //handle connection error
                        errorHandler: function (error) {
                            console.error(error);
                        }

                    });

                    return {
                        client: function (functionName, fn) {
                            hub.on(functionName, fn);
                        },
                        sendMessage: function (message) {
                            hub.sendMessage(message); //Calling a server method
                        }
                    };
                };
            }
        ]);
        ngapp.controller('DemoCtrl', [
            '$scope', 'AngularHub', function ($scope, AngularHub) {
                var hub = AngularHub(true);
                hub.client('broadcast', function (message) {
                    console.log('[DemoCtrl] ' + message);
                });
                $scope.send = function() {
                    hub.sendMessage($scope.message);
                };
            }
        ]);
        ngapp.controller('Demo2Ctrl', [
            '$scope', 'AngularHub', function ($scope, AngularHub) {
                var hub = AngularHub(true);
                hub.client('broadcast', function (message) {
                    console.log('[Demo2Ctrl] ' + message);
                });
                $scope.send = function () {
                    hub.sendMessage($scope.message);
                };
            }
        ]);
        ngapp.controller('Demo3Ctrl', [
            '$scope', 'AngularHub', function ($scope, AngularHub) {
                var hub = AngularHub();
                hub.client('broadcast', function (message) {
                    console.log('[Demo3Ctrl] ' + message);
                });
                $scope.send = function () {
                    hub.sendMessage($scope.message);
                };
            }
        ]);
    </script>
</body>
</html>
