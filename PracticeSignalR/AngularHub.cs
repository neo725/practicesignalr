﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR.Hubs;

namespace PracticeSignalR
{
    [HubName("AngularHub")]
    public class AngularHub : Hub
    {
        public void SendMessage(string message)
        {
            //Clients.All.broadcast(String.Format("server received >> {0}", message));
            Clients.Caller.broadcast(String.Format("server received >> {0}", message));
        }
    }
}