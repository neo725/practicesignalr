﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PracticeSignalR.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlNotLogin" runat="server">
            <asp:TextBox ID="txtAccountId" runat="server"></asp:TextBox>
            <asp:Button ID="btnLogin" runat="server" Text="Button" OnClick="btnLogin_Click" />
        </asp:Panel>
        <asp:Panel ID="pnlLogined" runat="server" Visible="False">
            <asp:Label ID="lblAccount" runat="server" Text="Label"></asp:Label>
            <asp:Button ID="btnLogout" runat="server" Text="Button" OnClick="btnLogout_Click" />
        </asp:Panel>
    </form>
</body>
</html>
